<?php

require_once __DIR__ . '/models/teamlead.php';
require_once __DIR__ . '/models/approved_payperiod.php';

$contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';

if($contentType === 'application/json'){
	$content = trim(file_get_contents('php://input'));
	$decoded = json_decode($content, true);

	$employee_pp = (object) [
		'employeeid' => trim($decoded['employeeid']),
		'pp_year' => trim($decoded['pp_year']),
		'pp_month' => trim($decoded['pp_month']),
		'pp_period' => trim($decoded['pp_period'])
	];
}else{
	$employee_pp = (object) [
		'employeeid' => isset($_GET['employeeid']) ? $_GET['employeeid'] : '',
		'pp_year' => isset($_GET['pp_year']) ? $_GET['pp_year'] : '',
		'pp_month' => isset($_GET['pp_month']) ? $_GET['pp_month'] : '',
		'pp_period' => isset($_GET['pp_period']) ? $_GET['pp_period'] : ''
	];
}

header('Content-Type: json/application');




$full_period = implode('-', [$employee_pp->pp_year, $employee_pp->pp_month, $employee_pp->pp_period]);


$approveDetails = ApprovedPayperiod::init()
                                   ->set_payperiod($full_period)
                                   ->set_employeeid($employee_pp->employeeid);

if($approveDetails->is_exist() > 0)
	echo json_encode(['exist' => 1]);
else
	echo json_encode(['exist' => 0]);


<?php

if(!isset($_SESSION)){
	session_start();
}

if(!isset($_SESSION['employeeid']))
	header("Location: /Internal/schedule/");

$tlid = $_SESSION['employeeid'];


require_once __DIR__ . '/models/timelog.php';
require_once __DIR__ . '/models/group_schedule.php';
require_once __DIR__ . '/models/employee.php';
require_once __DIR__ . '/models/user_login.php';
require_once __DIR__ . '/models/approved_payperiod.php';
require_once __DIR__ . '/models/functions.php';


$employee_pp = (object) [
	'employeeid' => isset($_GET['employeeid']) ? $_GET['employeeid'] : '',
	'pp_year' => isset($_GET['pp_year']) ? $_GET['pp_year'] : '',
	'pp_month' => isset($_GET['pp_month']) ? $_GET['pp_month'] : '',
	'pp_period' => isset($_GET['pp_period']) ? $_GET['pp_period'] : ''
];

// Current User Data
$user = UserLogin::createInstance()->setEmployeeCd($tlid);
$timelogStatuses = $user->userLoginLevelTimelogStatuses();

// Employee Data
$employee = Employee::find($employee_pp->employeeid);

// Schedule Data
$shifts = GroupSchedule::all();

// Timelog Data
$employee_timelog = new Timelog($employee_pp);
if($employee_timelog->isNotEmpty()){
	$employee_pp_timelog = $employee_timelog->get_payperiod_timelog();
	$calendar_header = $employee_timelog->getFirstLastDate();
	$summary_rows = $employee_timelog->get_hours_summary();
}else{
	$calendar_header = '';
	$employee_pp_timelog = '';
}

// Approved Payperiod Data
$full_pp = $employee_pp->pp_year . '-' . $employee_pp->pp_month . '-' . $employee_pp->pp_period;

$approved_pp = ApprovedPayperiod::init()
                                ->set_payperiod($full_pp)
                                ->set_employeeid($employee_pp->employeeid);

$is_approved = $approved_pp->is_exist() > 0 ? true : false;

include view('approver.employee_timelog_vw');
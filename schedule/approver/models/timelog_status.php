<?php
require_once __DIR__ . '/dbconnect.php';

class TimelogStatus{
	public $id;
	public $name;
	public $description;

	public function __construct(){

	}

	public static function createInstance(){
		return new self();
	}

	public function setId($id){
		$db = new DBConnect();

		$q = "SELECT id, name, description FROM timelog_statuses WHERE id = $id";
		$result = $db->conn->query($q);
		$row = $result->fetch_assoc();	

		if($row){
			$this->id = $row['id'];
			$this->name = $row['name'];
			$this->description = $row['description'];
		}else{
			$this->id = $id;
		}

		$db->close();

		return $this;
	}

}
<?php
require_once __DIR__ . '/dbconnect.php';

class HoursTable{

	public $shiftdate;
	public $dates;
	public $userid;
	public $early;
	public $regular_hours;
	public $nightdiff_hours;
	public $regular_ot_hours;
	public $nightdiff_ot_hours;
	public $late;
	public $undertime;
	public $sixth_ot_hours;
	public $seventh_ot_hours;
	public $status;

	public function __construct(){
		// these values are always zero
		$this->early = 0;
		$this->sixth_ot_hours = 0;
		$this->seventh_ot_hours = 0;
	}

	public static function init(){
		return new self;
	}

	public function set_shiftdate($shiftdate){
		$this->shiftdate = $shiftdate;
		return $this;
	}

	public function set_dates($dates){
		$this->dates = $dates;
		return $this;
	}

	public function set_userid($userid){
		$this->userid = $userid;
		return $this;
	}

	public function set_regularHours($regular_hours){
		$this->regular_hours = $regular_hours;
		return $this;
	}

	public function set_nightdiffHours($nightdiff_hours){
		$this->nightdiff_hours = $nightdiff_hours;
		return $this;
	}

	public function set_regularOtHours($regular_ot_hours){
		$this->regular_ot_hours = $regular_ot_hours;
		return $this;
	}

	public function set_nightdiffOtHours($nightdiff_ot_hours){
		$this->nightdiff_ot_hours = $nightdiff_ot_hours;
		return $this;
	}


	public function set_late($late){
		$this->late = $late;
		return $this;
	}

	public function set_undertime($undertime){
		$this->undertime = $undertime;
		return $this;
	}

	public function set_status($status){
		$this->status = $status;
		return $this;
	}


	public function insert(){
		$db = new DBConnect();
		$q = "INSERT INTO hourstable (shiftdate, 
			                          dates, 
			                          userid, 
			                          early, 
			                          regular_hours, 
			                          nightdiff_hours, 
			                          regular_ot_hours, 
			                          nightdiff_ot_hours, 
			                          late, 
			                          undertime, 
			                          sixth_ot_hours,
			                          seventh_ot_hours,
			                          status)
              VALUES                 ('{$this->shiftdate}',
                                      '{$this->dates}', 
			                          '{$this->userid}', 
			                          {$this->early}, 
			                          {$this->regular_hours}, 
			                          {$this->nightdiff_hours}, 
			                          {$this->regular_ot_hours}, 
			                          {$this->nightdiff_ot_hours}, 
			                          {$this->late}, 
			                          {$this->undertime}, 
			                          {$this->sixth_ot_hours},
			                          {$this->seventh_ot_hours},
			                          '{$this->status}')";

		$db->conn->query($q);
		$db->close();
	}

	public function update(){
		$db = new DBConnect();
		$q = "UPDATE hourstable 
		      SET shiftdate = '{$this->shiftdate}',
		          dates = '{$this->dates}',
		          userid = '{$this->userid}',
		          early = {$this->early},
		          regular_hours = {$this->regular_hours},
		          nightdiff_hours = {$this->nightdiff_hours},
		          regular_ot_hours = {$this->regular_ot_hours},
		          nightdiff_ot_hours = {$this->nightdiff_ot_hours},
		          late = {$this->late},
		          undertime = {$this->undertime},
		          sixth_ot_hours = {$this->sixth_ot_hours},
		          seventh_ot_hours = {$this->seventh_ot_hours},
		          status = '{$this->status}'
		     WHERE userid = '{$this->userid}'
		     AND shiftdate = '{$this->shiftdate}'
		     AND dates = '{$this->dates}'";

		$db->conn->query($q);
		$db->close();
	}

	public function delete(){
		$db = new DBConnect();
		$q = "DELETE FROM hourstable WHERE userid = '{$this->userid}' AND shiftdate = '{$this->shiftdate}' AND dates = '{$this->dates}'";

		$db->conn->query($q);
		$db->close();
	}

	public function is_exist(){
		$db = new DBConnect();
		$q = "SELECT 1 FROM hourstable WHERE userid = '{$this->userid}' AND shiftdate = '{$this->shiftdate}' AND dates = '{$this->dates}'";

		$db->conn->query($q);
		return $db->conn->affected_rows;
	}
}
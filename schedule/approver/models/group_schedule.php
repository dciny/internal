<?php
require_once __DIR__ . '/dbconnect.php';

class GroupSchedule{

	public static function all(){
		$db = new DBConnect();

		$q = "select groupschedulename from groupschedule order by groupschedulename";
		$result = $db->conn->query($q);
		if($result->num_rows)
			$data = $result->fetch_all(MYSQLI_ASSOC);	
		else
			$data = ['groupschedulename' => ''];

		$db->close();

		// return (object) $data;
		return array_map('self::to_groupSchedule_obj',$data);
	}

	public static function find($groupname){
		$db = new DBConnect();

		$q = "select * from groupschedule where groupschedulename = '$groupname'";
		$result = $db->conn->query($q);
		if($result->num_rows)
			$data = $result->fetch_assoc();	
		else
			$data = ['groupschedulename' => ''];

		$db->close();

		// return (object) $data;
		return self::to_groupSchedule_obj($data);
	}

	private static function to_groupSchedule_obj($sched){
		return (object) $sched;
	}

}
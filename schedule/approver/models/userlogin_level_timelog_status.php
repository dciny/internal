<?php
require_once __DIR__ . '/dbconnect.php';
require_once __DIR__ . '/timelog_status.php';

class UserloginLevelTimelogStatus{
	
	public static function filterByUserLevel($level){
		$db = new DBConnect();

		$q = "SELECT id, timelog_status_id, userlogin_level FROM userlogin_level_timelog_statuses WHERE userlogin_level = $level";
		$result = $db->conn->query($q);
		$rows = $result->fetch_all(MYSQLI_ASSOC);	

		$db->close();

		$timelog_statuses = [];
		foreach($rows as $row){
			array_push($timelog_statuses, self::timelogStatus($row['timelog_status_id']));
		}
		return $timelog_statuses;
	}


	// Association
	private function timelogStatus($timelog_status_id){
		return TimelogStatus::createInstance()->setId($timelog_status_id);
	}

}
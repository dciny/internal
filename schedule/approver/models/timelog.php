<?php
require_once __DIR__ . '/dbconnect.php';
require_once __DIR__ . '/functions.php';


class Timelog{
	public $employeeid;
	public $pp_year;
	public $pp_month;
	public $pp_period;

	public function __construct($employee){
		$this->employeeid = $employee->employeeid;
		$this->pp_year = $employee->pp_year;
		$this->pp_month = $employee->pp_month;
		$this->pp_period = $employee->pp_period;
	}

	public static function min_date(){
		$db = new DBConnect();
		$q = "SELECT MIN(dates) as min_date FROM `timelog` where dates != '0000-00-00'";

		$result = $db->conn->query($q);
		$data = $result->fetch_assoc();	
		if(!empty($data->min_date)){
			$data = (object) $data;
		}else{
			$data = (object)['min_date' => date('Y-m-d')];
		}

		$db->close();

		return $data;
	}

	public function get_payperiod_timelog(){
		$period_days = $this->period_days();
		$sched = $this->assigned_sched();
		$timelog = $this->generate_timelog($period_days, $sched);
		
		return $timelog;
	}


	public function hourstable_to_insert(){
		$timelogs = $this->get_payperiod_timelog();

		$rows = [];
		foreach ($timelogs as $timelog) {
			$timelog_exist = $this->timelog_exist($timelog->day);
			if(!empty($timelog_exist->dates)){
				$timelog = $timelog_exist;
				$assigned_schedule = $this->assigned_sched(trim($timelog->thisdaysshift));
			}else{
				$assigned_schedule = $this->assigned_sched();
			}

			$day = $timelog->day;

			$schedule = $this->get_scheduled_timein_out($timelog->startshift, $timelog->endshift, $assigned_schedule);
			if($timelog->thisdaysshift == 'Flexible'){
				$late_hrs = 0;
				$undertime_hrs = 0;
			}else{
				$late_hrs = $this->statusHourCountable($timelog->status,$this->calculate_late_hours($timelog->startshift, $schedule->timein));
				$undertime_hrs = $this->statusHourCountable($timelog->status,$this->calculate_undertime_hours($timelog->startshift, $timelog->endshift));
			}
			
			// get Night diff and regular hours for normal shift
			$shifthours = [];
			$this->calcEveningND($shifthours, $timelog->startshift, $timelog->endshift);
			$this->calcMorningND($shifthours, $timelog->startshift, $timelog->endshift);
			$this->calcRegHrs($shifthours, $timelog->startshift, $timelog->endshift);

			// get Night diff and regular hours for overtime
			if($timelog->otstart == '0000-00-00 00:00:00' || $timelog->otstart == '1900-01-01 00:00:00' ||
			   $timelog->otend == '0000-00-00 00:00:00' || $timelog->otend == '1900-01-01 00:00:00'){
				$othours[$day] = [
					'ndHrs' => 0,
					'regHrs' => 0
			    ];
			}else{
				$othours = [];
				$this->calcEveningND($othours, $timelog->otstart, $timelog->otend);
				$this->calcMorningND($othours, $timelog->otstart, $timelog->otend);
				$this->calcRegHrs($othours, $timelog->otstart, $timelog->otend);
			}

			$dayPlus1 = date('Y-m-d', strtotime($day . '+1 day'));
			$iterations = [$day, $dayPlus1];

			$late = [
				$day => $late_hrs,
				$dayPlus1 => 0
			];

			$undertime = [
				$day => $undertime_hrs,
				$dayPlus1 => 0
			];

			foreach ($iterations as $iteration){
				if($timelog->status == 'Absent'){
					$regular_hours = 0;
					$nightdiff_hours = 0;
					$regular_ot_hours = 0;
					$nightdiff_ot_hours = 0;
				}else{
					$regular_hours = isset($shifthours[$iteration]['regHrs']) ? $shifthours[$iteration]['regHrs'] : 0;
					$nightdiff_hours = isset($shifthours[$iteration]['ndHrs']) ? $shifthours[$iteration]['ndHrs'] : 0;
					$regular_ot_hours = isset($othours[$iteration]['regHrs']) ? $othours[$iteration]['regHrs'] : 0;
					$nightdiff_ot_hours = isset($othours[$iteration]['ndHrs']) ? $othours[$iteration]['ndHrs'] : 0;
				}

				array_push($rows, (object)[
					'shiftdate' => $day,
					'dates' => $iteration,
					'userid' => $timelog->userid,
					'early' => 0,
					'regular_hours' => $regular_hours,
					'nightdiff_hours' => $nightdiff_hours,
					'regular_ot_hours' => $regular_ot_hours,
					'nightdiff_ot_hours' => $nightdiff_ot_hours,
					'late' => $late[$iteration],
					'undertime' => $undertime[$iteration],
					'sixth_ot_hours' => 0,
					'seventh_ot_hours' => 0,
					'status' => $timelog->status
				]);
			}

		}

		return $rows;
	}


	public function get_hours_summary(){
		$timelogs = $this->get_payperiod_timelog();

		$summary_rows = [];
		foreach ($timelogs as $timelog) {
			$timelog_exist = $this->timelog_exist($timelog->day);
			if(!empty($timelog_exist->dates)){
				$timelog = $timelog_exist;
				$assigned_schedule = $this->assigned_sched(trim($timelog->thisdaysshift));
			}else{
				$assigned_schedule = $this->assigned_sched();
			}

			$day = $timelog->day;

			$schedule = $this->get_scheduled_timein_out($timelog->startshift, $timelog->endshift, $assigned_schedule);
			if($timelog->thisdaysshift == 'Flexible'){
				$late_hrs = 0;
				$undertime_hrs = 0;
			}else{
				$late_hrs = $this->statusHourCountable($timelog->status,$this->calculate_late_hours($timelog->startshift, $schedule->timein));
				$undertime_hrs = $this->statusHourCountable($timelog->status,$this->calculate_undertime_hours($timelog->startshift, $timelog->endshift));
			}
			
			// get Night diff and regular hours for normal shift
			$shifthours = [];
			$this->calcEveningND($shifthours, $timelog->startshift, $timelog->endshift);
			$this->calcMorningND($shifthours, $timelog->startshift, $timelog->endshift);
			$this->calcRegHrs($shifthours, $timelog->startshift, $timelog->endshift);
			$sumShiftHrs = $this->sumNDAndRegHours($shifthours);

			// get Night diff and regular hours for overtime
			if($timelog->otstart == '0000-00-00 00:00:00' || $timelog->otstart == '1900-01-01 00:00:00' ||
			   $timelog->otend == '0000-00-00 00:00:00' || $timelog->otend == '1900-01-01 00:00:00'){
				$sumOtHrs = [
					'ndHrs' => 0,
					'regHrs' => 0
			    ];
			}else{
				$othours = [];
				$this->calcEveningND($othours, $timelog->otstart, $timelog->otend);
				$this->calcMorningND($othours, $timelog->otstart, $timelog->otend);
				$this->calcRegHrs($othours, $timelog->otstart, $timelog->otend);

				$sumOtHrs = $this->sumNDAndRegHours($othours);
			}


			array_push($summary_rows, (object)[
				'status' => $timelog->status,
				'day' => $day,
				'late' => $late_hrs,
				'undertime' => $undertime_hrs,
				'regHrs' => $this->statusHourCountable($timelog->status,$sumShiftHrs['regHrs']),
				'withND' => $this->statusHourCountable($timelog->status,$sumShiftHrs['ndHrs']),
				'regOt' => $this->statusHourCountable($timelog->status,$sumOtHrs['regHrs']),
				'otWithND' => $this->statusHourCountable($timelog->status,$sumOtHrs['ndHrs'])
			]);
		}

		return $summary_rows;
	}

	private function sumNDAndRegHours($hours){
		$ndHrs = 0;
		$regHrs = 0;
		foreach($hours as $hour){
		  $ndval = isset($hour['ndHrs']) ? $hour['ndHrs'] : 0;
		  $regval = isset($hour['regHrs']) ? $hour['regHrs'] : 0;
		  
		  $ndHrs = $ndHrs + $ndval;
		  $regHrs = $regHrs + $regval;
		}

		return [
			'ndHrs' => $ndHrs,
			'regHrs' => $regHrs
		];
	}

	public function calcEveningND(&$hourStorage, $startdate, $enddate){
		$dateAt10Pm = date('Y-m-d 22:00:00', strtotime($startdate));
		$dateAt12Am = date('Y-m-d 00:00:00', strtotime($dateAt10Pm . '+2 hours'));
		$enddate = date('Y-m-d H:i:s', strtotime($enddate)); // make sure enddate is of type date

		$hour = date('Y-m-d H:i:s', strtotime($startdate)); // counter to use

		while($hour < $dateAt10Pm && $hour <= $enddate)
			$hour = date('Y-m-d H:i:s', strtotime($hour . '+1 hour'));

		if($hour >= $dateAt10Pm && $hour <= $dateAt12Am){
			$hour = $startdate < $hour ? $dateAt10Pm : $hour;
			$dateAt12Am = $dateAt12Am <= $enddate ? $dateAt12Am : $enddate;
			$offset = round((strtotime($dateAt12Am) - strtotime($hour)) / 3600, 2);
			$date_idx = date('Y-m-d', strtotime($startdate));

			if(isset($hourStorage[$date_idx]) && isset($hourStorage[$date_idx]['ndHrs'])){
				$hourStorage[$date_idx]['ndHrs'] = $hourStorage[$date_idx]['ndHrs'] + $offset;
			}else{
				$hourStorage[$date_idx] = ['ndHrs' => $offset];
			}
		}
	}

	public function calcMorningND(&$hourStorage, $startdate, $enddate){
		$startdate = date('Y-m-d H:i:s', strtotime($startdate));
	    $enddate = date('Y-m-d H:i:s', strtotime($enddate));
	    
	    $dateAt12Am = date('Y-m-d 00:00:00', strtotime($enddate));
	    $dateAt6Am = date('Y-m-d H:i:s', strtotime($dateAt12Am . '+6 hours'));
	    
	    $offset = 0;
	    if($startdate < $dateAt6Am){
	         $from = $startdate > $dateAt12Am ? $startdate : $dateAt12Am;
	         $to = $enddate > $dateAt6Am ? $dateAt6Am : $enddate;
	         
	         $offset = round((strtotime($to) - strtotime($from)) / 3600, 2);
	         $date_idx = date('Y-m-d', strtotime($enddate));
	         
	         if(isset($hourStorage[$date_idx]['ndHrs'])){
				$hourStorage[$date_idx]['ndHrs'] = $hourStorage[$date_idx]['ndHrs'] + $offset;
			}else{
				$hourStorage[$date_idx] = ['ndHrs' => $offset];
			}
	    }
	}

	public function calcRegHrs(&$hourStorage, $startdate, $enddate){
		$startdate = date('Y-m-d H:i:s', strtotime($startdate));
		$enddate = date('Y-m-d H:i:s', strtotime($enddate));

		$dateAt6Am = date('Y-m-d 06:00:00', strtotime($startdate));
		$dateAt10Pm = date('Y-m-d 22:00:00', strtotime($startdate));
		$dateAt12Am = date('Y-m-d H:i:s', strtotime($dateAt10Pm . '+2 hours'));

		$from = $startdate < $dateAt6Am ? $dateAt6Am : $startdate;
		$until = $enddate < $dateAt10Pm ? $enddate : $dateAt10Pm;

		$offset = round((strtotime($until) - strtotime($from)) / 3600, 2);
		$offset = $offset < 0 ? 0 : $offset;
		$date_idx = date('Y-m-d', strtotime($startdate));

		if(isset($hourStorage[$date_idx]['regHrs'])){
		  $hourStorage[$date_idx]['regHrs'] = $hourStorage[$date_idx]['regHrs'] + $offset;
		}else{
		  $hourStorage[$date_idx]['regHrs'] = $offset;
		}


		if($enddate > $dateAt12Am){
		  $dateAt6am = date('Y-m-d H:i:s', strtotime($dateAt12Am . '+6 hours'));
		  if($enddate > $dateAt6am){
		     $offset = round((strtotime($enddate) - strtotime($dateAt6am)) / 3600,2);
		     $date_idx = date('Y-m-d', strtotime($enddate));
		     if(isset($hourStorage[$date_idx]['regHrs'])){
		        $hourStorage[$date_idx]['regHrs'] = $hourStorage[$date_idx]['regHrs'] + $offset;
		     }else{
		        $hourStorage[$date_idx]['regHrs'] = $offset;
		     }
		  }
		}
	}

	private function statusHourCountable($status, $hour){
		$countable = [
			'Present',
			'6th day OT',
			'7th day OT',
			'Vacation Leave',
			'Paid Timeoff',
			'Sick Leave',
			'BVMT Leave',
			'LPAT',
			'BVMT',
			'LMAT',
			'Emergency Leave',
			'Holiday Off'
		];

		if(in_array($status, $countable))
			return $hour;

		return 0;
	}

	public function	get_scheduled_timein_out($start, $end, $sched){
		$start = date('Y-m-d',strtotime($start)) . ' ' . $sched->starttm;
		$end = date('Y-m-d',strtotime($end)) . ' ' . $sched->endtm;

		return (object) [
			'timein' => $start,
			'timeout' => $end
		];
		
	}

	public function calculate_late_hours($startshift, $schedStartShift){
		$startshift  = strtotime($startshift);
		$schedStartShift = strtotime($schedStartShift);

		return round(($startshift - $schedStartShift) / 3600,2);
	}

	public function calculate_undertime_hours($startShift, $endShift){
		$startShift  = strtotime($startShift);
		$endShift = strtotime($endShift);

		$undertime = 9 - (round(($endShift - $startShift) / 3600,2));
		
		return $undertime < 0 ? 0 : $undertime;
	}

	

	// get the days given the period
	private function period_days(){
		$pp_month = $this->pp_month;
		$pp_year = $this->pp_year;

		switch ($this->pp_period) {
			case '10':
				if($this->pp_month == 1){
					$pp_year = $this->pp_year - 1;
					$pp_month = 12;
				}else{
					$pp_month = $this->pp_month - 1;
				}
				$start_day = 16;
				$end_day = date('t',strtotime(implode('-', [$pp_year,$pp_month,'01'])));
				break;
			
			case '25':
				$start_day = 1;
				$end_day = 15;
				break;
		}

		return $this->generate_dates($pp_month, $pp_year, $start_day, $end_day);

	}

	// generate the dates given the days from period_days
	private function generate_dates($pp_month, $pp_year, $start_day, $end_day){
		$dates = [];
		for($day = $start_day; $day <= $end_day; $day++){
			array_push($dates, implode('-', array($pp_year,str_pad($pp_month, 2, '0', STR_PAD_LEFT),str_pad($day, 2, '0', STR_PAD_LEFT))));
		}

		return $dates;
	}

	private function assigned_sched($groupsched=null){
		$db = new DBConnect();

		$q = "
			select g.groupschedulename,
				   g.starttime,
			       g.endtime,
			       g.lunchstart,
			       g.lunchend,
			       g.mon,
			       g.tue,
			       g.wed,
			       g.thu,
			       g.fri,
			       g.sat,
			       g.sun
			from groupschedule g
		";

		$join = empty($groupsched) ? " join prlemployeemaster p on p.schedule = g.groupschedulename " : "";
		$where = empty($groupsched) ? " where p.employeeid = '{$this->employeeid}'" : " where g.groupschedulename = '{$groupsched}' ";

		$q = $q . $join . $where;

		$result = $db->conn->query($q);
		$data = $result->fetch_assoc();

		$week_sched = [];
		if($data['mon']) array_push($week_sched, 1);
		if($data['tue']) array_push($week_sched, 2);
		if($data['wed']) array_push($week_sched, 3);
		if($data['thu']) array_push($week_sched, 4);
		if($data['fri']) array_push($week_sched, 5);
		if($data['sat']) array_push($week_sched, 6);
		if($data['sun']) array_push($week_sched, 7);

		$sched = (object)[
			'sched_code' => $data['groupschedulename'],
			'starttm' => $data['starttime'],
			'endtm' => $data['endtime'],
			'week_sched' => $week_sched
		];

		$db->close();

		return $sched;
	}

	private function generate_timelog($period_days, $sched){
		$timelog = [];

		foreach ($period_days as $day){
			$existing_timelog = $this->timelog_exist($day);
			if(!empty($existing_timelog->dates)){
				array_push($timelog, $existing_timelog);
				continue;
			}

			array_push($timelog, (object) [
				'userid' => $this->employeeid,
				'dates' => date('j',strtotime($day)),
				'shiftday' => date('D',strtotime($day)), // Mon, Tue, Wed, etc.
				'status' => in_array(date('N',strtotime($day)), $sched->week_sched) ? 'Present' : 'Restday', // 1 = monday, 7 = sunday
				'startshift' => $day . ' ' . $sched->starttm,
				'endshift' => $this->enddate($day, $sched->starttm, $sched->endtm),
				'otstart' => "0000-00-00 00:00:00",
				'otend' => "0000-00-00 00:00:00",
				'thisdaysshift' => $sched->sched_code,
				'day' => $day
			]);	
		}

		return $timelog;
	}

	private function enddate($day, $starttm, $endtm){
		if($endtm < $starttm){
			return implode(' ',[date('Y-m-d', strtotime($day . '+1 day')), $endtm]);
		}
		return implode(' ', [$day, $endtm]);
	}

	public function timelog_exist($shiftdate){
		$db = new DBConnect();

		$employeeid = $this->employeeid;

		$q = "SELECT userid, dates, shiftday, status, startshift, endshift, otstart, otend, thisdaysshift FROM timelog WHERE userid = '$employeeid' AND dates = '$shiftdate'";
		$result = $db->conn->query($q);
		if($result->num_rows){
			$data = $result->fetch_assoc();

			return (object) [
				'userid' => $data['userid'],
				'dates' => date('j',strtotime($data['dates'])),
				'shiftday' => $data['shiftday'],
				'status' => $data['status'],
				'startshift' => $data['startshift'],
				'endshift' => $data['endshift'],
				'otstart' => $data['otstart'],
				'otend' => $data['otend'],
				'thisdaysshift' => $data['thisdaysshift'],
				'day' => $data['dates']
			];
		}

		return (object) ['dates' => ''];
	}


	public function getFirstLastDate(){
		$dates = $this->period_days();

		return (object) [
			'first' => date('j',strtotime(reset($dates))),
			'last' => date('j',strtotime(end($dates))),
			'month' => date('F', strtotime(end($dates))),
			'year' => date('Y', strtotime(end($dates))),
		];
	}

	public function isNotEmpty(){
		if(empty($this->employeeid) || empty($this->pp_year) || empty($this->pp_month) || empty($this->pp_period))
			return false;

		return true;
	}

	public static function insertOrUpdate($params){
		if(self::is_timelog_exist($params))
			self::update_timelog($params);
		else
			self::create_timelog($params);	
	}

	private static function is_timelog_exist($params){
		$db = new DBConnect();

		$q = "SELECT 1 FROM timelog WHERE userid = '{$params->userid}' AND dates = '{$params->dates}'";

		$db->conn->query($q);

		return $db->conn->affected_rows;
	}
	
	private static function update_timelog($params){
		$db = new DBConnect();

		$q = "
			UPDATE timelog
			SET status = '{$params->status}',
			    skedin = '{$params->skedin}',
			    skedout = '{$params->skedout}',
			    startshift = '{$params->startshift}',
			    endshift = '{$params->endshift}',
			    otstart = '{$params->otstart}',
			    otend = '{$params->otend}',
			    thisdaysshift = '{$params->thisdayshift}'
			WHERE userid = '{$params->userid}'
			AND dates = '{$params->dates}'
		";

		$db->conn->query($q);

		return $db->conn->affected_rows;
	}

	private static function create_timelog($params){
		$db = new DBConnect();

		$q = "
			INSERT INTO timelog (dates, shiftday, status, userid, skedin, skedout, startshift, endshift, otstart, otend, thisdaysshift)
			VALUES ('{$params->dates}','{$params->shiftday}', '{$params->status}', '{$params->userid}', '{$params->skedin}', '{$params->skedout}', '{$params->startshift}', '{$params->endshift}', '{$params->otstart}', '{$params->otend}', '{$params->thisdayshift}')
		";

		$db->conn->query($q);

		return $db->conn->affected_rows;
	}
}

<?php
require_once __DIR__ . '/dbconnect.php';

class Employee{
	public $employeeid;

	public function __construct($employeeid){
		$this->employeeid = $employeeid;
	}

	public function details(){
		$db = new DBConnect();

		$q = "select * from prlemployeemaster where employeeid = '{$this->employeeid}'";
		$result = $db->conn->query($q);
		$data = $result->fetch_assoc();	

		$db->close();

		return (object) $data;
	}

	public static function find($employeeid){
		$db = new DBConnect();

		$q = "select * from prlemployeemaster where employeeid = '$employeeid'";
		$result = $db->conn->query($q);
		if($result->num_rows)
			$data = $result->fetch_assoc();	
		else
			$data = self::empty_employee();

		$db->close();

		return (object) $data;
	}

	private function empty_employee(){
		return [
			'employeeid' => '',
			'firstname' => '',
			'lastname' => ''
		];
	}


}
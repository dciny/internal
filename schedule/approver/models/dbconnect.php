<?php

class DBConnect{
	public $conn;

	public function __construct(){
		$host = 'localhost';
		$user = 'WebDevTeam';
		$pw = 'L34dG3n789';
		$db = 'internal';

		$this->conn = new mysqli($host, $user, $pw, $db);

		if (mysqli_connect_error()) {
		  die("Database connection failed: " . mysqli_connect_error());
		}
	}

	public function close(){
		$this->conn->close();
	}
}
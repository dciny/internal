<?php
require_once __DIR__ . '/dbconnect.php';
require_once __DIR__ . '/employee.php';
require_once __DIR__ . '/functions.php';


class Teamlead{

	public $tlid;

	public function __construct($tlid){
		$this->tlid = $tlid;
	}

	public function members(){
		$db = new DBConnect();
		$q = "select t.employeeid from teamassignment t 
		      left join prlemployeemaster p on p.employeeid = t.employeeid
			  where p.employeeid is not null and teamlead = {$this->tlid}";

		$result = $db->conn->query($q);
		$data = $result->fetch_all(MYSQLI_ASSOC);	


		$db->close();

		return array_map(array($this,'to_employee_object'), $data);
	}


	private function to_employee_object($emp){
		return Employee::find($emp['employeeid']);
	}
}
<?php
require_once __DIR__ . '/dbconnect.php';
require_once __DIR__ . '/userlogin_level_timelog_status.php';

class UserLogin{
	public $level;
	public $name;
	public $employee_cd;

	public function __construct(){
		$this->level = 0;
		$this->name= '';
		$this->employee_cd = 0;
	}

	public static function createInstance(){
		return new self();
	}

	public function setLevel($level){
		$this->level = $level;
		return $this;
	}

	public function setName($name){
		$this->name = $name;
		return $this;
	}

	public function setEmployeeCd($employee_cd){
		$db = new DBConnect();

		$q = "SELECT level, name, employeeid FROM userlogin WHERE employeeid = $employee_cd";
		$result = $db->conn->query($q);
		$row = $result->fetch_assoc();	

		if($row){
			$this->level = $row['level'];
			$this->name = $row['name'];
			$this->employee_cd = $row['employeeid'];
		}else{
			$this->employee_cd = $employee_cd;
		}

		$db->close();

		return $this;
	}

	// Association
	public function userLoginLevelTimelogStatuses(){
		return UserloginLevelTimelogStatus::filterByUserLevel($this->level);
	}

}
<?php
require_once __DIR__ . '/dbconnect.php';

class ApprovedPayperiod{
	public $payperiod;
	public $employeeid;
	public $tlApproved;
	public $wfmApproved;

	public function __construct(){
		$this->wfmApproved = 0;
		$this->tlApproved = 1;
	}

	public static function init(){
		return new self;
	}

	public function set_payperiod($payperiod){
		$this->payperiod = $payperiod;
		return $this;
	}

	public function set_employeeid($employeeid){
		$this->employeeid = $employeeid;
		return $this;
	}

	public function set_tlApproved($tlApproved){
		$this->tlApproved = $tlApproved;
		return $this;
	}

	public function set_wfmApproved($wfmApproved){
		$this->wfmApproved = $wfmApproved;
		return $this;
	}

	public function insert(){
		$db = new DBConnect();

		$q = "INSERT INTO approved_payperiods (payperiod, employeeid, TLapproved, WFMapproved)
		      VALUES ('{$this->payperiod}','{$this->employeeid}',{$this->tlApproved},{$this->wfmApproved})";

		$db->conn->query($q);
		$db->close();
	}

	public function update(){
		$db = new DBConnect();

		$q= "UPDATE approved_payperiods
		     SET TLapproved = {$this->tlApproved},
		         WFMapproved = {$this->wfmApproved}
		     WHERE payperiod = '{$this->payperiod}'
		     AND employeeid = '{$this->employeeid}'";

		$db->conn->query($q);
		$db->close();
	}

	public function delete(){
		$db = new DBConnect();

		$q = "DELETE FROM approved_payperiods WHERE payperiod = '{$this->payperiod}' AND employeeid = '{$this->employeeid}'";

		$db->conn->query($q);
		$db->close();
	}

	public function is_exist(){
		$db = new DBConnect();
		$q = "SELECT 1 FROM approved_payperiods WHERE payperiod = '{$this->payperiod}' AND employeeid = '{$this->employeeid}'";

		$db->conn->query($q);
		return $db->conn->affected_rows;
	}

}
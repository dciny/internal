<?php

function array_to_object($arr){
	return (object) $arr;
}

function set_session(){
	if(!isset($_SESSION)) 
		session_start();
}

function year_from_to_current($from){
	date_default_timezone_set('Asia/Kuala_Lumpur');

	$year_from = date('Y',strtotime($from));
	$year_curr = date('Y');

	$years = [];
	for($year = $year_from; $year <= $year_curr; $year++)
		array_push($years, $year);

	return $years;
}

function month_options(){
	return [
		(object)['mo' => '01','month' => 'January'],
		(object)['mo' => '02','month' => 'February'],
		(object)['mo' => '03','month' => 'March'],
		(object)['mo' => '04','month' => 'April'],
		(object)['mo' => '05','month' => 'May'],
		(object)['mo' => '06','month' => 'June'],
		(object)['mo' => '07','month' => 'July'],
		(object)['mo' => '08','month' => 'August'],
		(object)['mo' => '09','month' => 'September'],
		(object)['mo' => '10','month' => 'October'],
		(object)['mo' => '11','month' => 'November'],
		(object)['mo' => '12','month' => 'December']
	];
}

function selected($val1, $val2){
	return $val1 == $val2 ? 'selected' : '';
}

function view($view_path){

	// If using the slash (/) format. Ex. view(folder_name/view_name)
	if(stripos($view_path, '/'))
		return str_replace('.php.php','.php',str_replace('//','/', dirname(dirname(__FILE__)) . '/views/' . $view_path . '.php'));

	// If using the dot (.) format. Ex. view(folder_name.view_name)
	return dirname(dirname(__FILE__)) . '/views/' . str_replace('/php','',implode('/',explode('.', $view_path)) . '.php');
}


function day_status_style($status){
	switch ($status) {
		case 'Restday':
			return 'restday';
		case 'Absent':
			return 'absent';
		case '6th day OT':
		case '7th day OT':
			return 'ot';
	}
}

function datetimeLocal($datetime){
	$date = date('Y-m-d',strtotime($datetime));
	$time = date('H:i:s',strtotime($datetime));

	return $date . 'T' . $time;
}

function displayDateThatCanBeEmpty($date){
	if($date == '0000-00-00 00:00:00' || $date == '1900-01-01 00:00:00')
		return '0000-00-00 00:00:00';
	return $date;
}

// function htmlForDateThatCanBeEmpty($date, $name, $class){
// 	if($date == '0000-00-00 00:00:00' || $date == '1900-01-01 00:00:00')
// 		return "<input type='datetime-local' name='$name' class='$class'>";

// 	$date = datetimeLocal($date);
// 	return "<input type='datetime-local' name='$name' value='$date'>";
// }

function htmlForDateThatCanBeEmpty($date, $name, $class){
	if($date == '0000-00-00 00:00:00' || $date == '1900-01-01 00:00:00'){
		return "<input type='text' class='form-control datetmpicker'>";
	}

	return "<input type='text' class='form-control datetmpicker' value='$date'";
}

function standardDateFormat($date){
	if($date == '0000-00-00 00:00:00' || $date == '1900-01-01 00:00:00')
		return '1900-01-01 00:00:00';
	return $date;
}

function is_editable($status){
	return in_array($status, ['Present','Absent','Restday','6th day OT','7th day OT']) ? true : false;
}
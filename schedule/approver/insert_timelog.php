<?php

if(!isset($_SESSION)){
	session_start();
}

if(!isset($_SESSION['employeeid']))
	header("Location: /Internal/schedule/");


require_once __DIR__ . '/models/timelog.php';

header('Content-Type: json/application');

$contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';

if($contentType === 'application/json')
	$content = trim(file_get_contents('php://input'));

$decoded = json_decode($content, true);

$params = (object)[
	'dates' => trim($decoded['day']),
	'shiftday' => trim($decoded['shiftday']),
	'status' => trim($decoded['status']),
	'userid' => trim($decoded['userid']),
	'skedin' => trim($decoded['startshift']),
	'skedout' => trim($decoded['endshift']), 
	'startshift' => trim($decoded['startshift']),
	'endshift' => trim($decoded['endshift']),
	'otstart' => trim($decoded['otstart']),
	'otend' => trim($decoded['otend']),
	'thisdayshift' => $decoded['shift']
];

Timelog::insertOrUpdate($params);

echo json_encode(['execution' => 'done']);

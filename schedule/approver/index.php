<?php

if(!isset($_SESSION)){
	session_start();
}

if(!isset($_SESSION['employeeid'])){
	header("Location: /Internal/schedule/");
}else{
	$tlid = $_SESSION['employeeid'];
}

require_once __DIR__ . '/models/teamlead.php';
require_once __DIR__ . '/models/employee.php';
require_once __DIR__ . '/models/timelog.php';
require_once __DIR__ . '/models/functions.php';
require_once __DIR__ . '/models/approved_payperiod.php';

$month_param = isset($_GET['month']) ? $_GET['month'] : date('m');
$year_param = isset($_GET['year']) ? $_GET['year'] : date('Y');
$period_param = isset($_GET['period']) ? $_GET['period'] : 10;


$tl = new Teamlead($tlid);
$members = $tl->members();

$members_with_approved_sched = [];
$full_period = implode('-', [$year_param, $month_param, $period_param]);
foreach ($members as $member) {
	$approveDetails = ApprovedPayperiod::init()
	                                   ->set_payperiod($full_period)
	                                   ->set_employeeid($member->employeeid);

	if($approveDetails->is_exist() > 0)
		array_push($members_with_approved_sched, $member->employeeid);
}

$month_options = month_options();
$timelog_min_year = Timelog::min_date()->min_date;
$year_options = year_from_to_current($timelog_min_year);

include view('approver.index_vw');


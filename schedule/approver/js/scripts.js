$(document).ready(()=>{

	// Initialize datetime: https://www.jqueryscript.net/time-clock/Clean-jQuery-Date-Time-Picker-Plugin-datetimepicker.html
	if(document.querySelector('.datetmpicker')){
		$('.datetmpicker').datetimepicker({
			format: 'Y-m-d H:i:s',
			step: 30,
			theme: 'dark'
		})
	}

	// Tooltip initialization
	$('[data-toggle="tooltip"]').tooltip();

	$('.input > span, .input-empty > span').on('dblclick',function(){
		$(this).parents('.removeOt').removeClass('removeOt')
		$(this).siblings('div').removeClass('hidden')
		$(this).addClass('hidden')

		$(this).siblings('div').find('.datetmpicker, select').focus()
	})

	$(document).on('blur','.input .datetmpicker', function(){
		let val = $(this).val()
		assignDateToLocalDateTime($(this), val)
	})

	$('.input select').on('blur change', function(){
		let value = $(this).val()

		$(this).parent().parent().addClass('hidden')
		$(this).parent().parent().siblings('span')
		                         .text(value)
		                         .removeClass('hidden')

		switch(value){
			case 'Absent':
				$(this).parents('tr').removeClass(['restday','ot','absent'])
				                     .addClass('absent')
				break
			case 'Restday':
				$(this).parents('tr').removeClass(['restday','ot','absent'])
				                     .addClass('restday')
				break
			case '6th day OT':
			case '7th day OT':
				$(this).parents('tr').removeClass(['restday','ot','absent'])
				                     .addClass('ot')
				break
			default:
				$(this).parents('tr').removeClass(['restday','ot','absent'])
		}
	})

	$(document).on('blur','.input-empty .datetmpicker', function(){
		let val = $(this).val()
		
		if(val){
			assignDateToLocalDateTime($(this), val)
			$(this).parents('td').addClass('removeOt')
		}
		else{
			$(this).parent().parent().addClass('hidden')
			$(this).parent().parent().siblings('span')
		                             .removeClass('hidden')
		}
	})

	$(document).on('click','.OtEl', function(){
		let base_parent = $(this).parents('tr')

		base_parent.find('.removeOt').each(dateReset)
		
	})

	function assignDateToLocalDateTime(el, val){
		el.parent().parent().addClass('hidden')

		// update actual input date
		let sanitizedDt = sanitize_date_format(val)
		el.siblings('[type=hidden]').val(sanitizedDt)

		// update datetime picker
		el.val(sanitizedDt)
		
		// update span
		let date = sanitizedDt == '' ? '0000-00-00 00:00:00' : sanitizedDt
		el.parent().parent().siblings('span')
		                    .text(date)
		                    .removeClass('hidden')
	}

	function sanitize_date_format(val){
		// let sanitized_date = val.match(/(?:\d{4}\-\d{1,2}\-\d{1,2} \d{1,2}\:\d{1,2})(?:\:\d{1,2})?/)
		let sanitized_date = val.match(/(?:\d{4}\-\d{1,2}\-\d{1,2})(?:(?: \d{1,2})?(\:\d{1,2}){0,2})?/)

		if(typeof sanitized_date[0] !== 'undefined')
			return sanitized_date[0]

		return '';
	}

	function dateReset(idx, el){
		$(el).find('span').text('0000-00-00 00:00:00')
		$(el).find('.datetmpicker').val('')
		$(el).find('[type=hidden]').val('1900-01-01 00:00:00')
		$(el).removeClass('removeOt')
	}


	$(document).on('click','#create-timelog-btn',async function(){
		$('.modal-container').removeClass('hidden');

		let timelogEl = $('#timelog-tbl').children()
		timelogs = [];


		timelogEl.each(function(){
			let status = $(this).find('[data-status]').val()
			let startshift = $(this).find('[data-startshift]').val()
			let endshift = $(this).find('[data-endshift]').val()
			let otstart = $(this).find('[data-otstart]').val()
			let otend = $(this).find('[data-otend]').val()
			let shift = $(this).find('[data-shift]').val()
			let day = $(this).find('[data-day]').val()
			let shiftday = $(this).find('[data-shiftday]').val()
			let userid = $(this).find('[data-userid]').val()


			timelogs.push({
				userid: userid,
				day: day,
				shiftday: shiftday,
				status: status,
				startshift: startshift,
				endshift: endshift,
				otstart: otstart,
				otend: otend,
				shift: shift
			})

		})


		for(let idx in timelogs){
			await insertTimelog(timelogs[idx])
		}


		location.reload();
		
	})

	async function insertTimelog(timelog){
		// get base url. Remove this if approver folder is the root folder
		let url = getBaseUrl() + 'insert_timelog.php';

		await fetch(url,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(timelog)
		})

		// $.ajax({
		// 	url: url,
		// 	type: 'post',
		// 	data: JSON.stringify(timelog),
		// 	contentType: 'application/json'
		// })
	}


	$(document).on('click','#approve-sched', function(){
		$('.modal-container').removeClass('hidden');

		let employeeid = $('[name=employeeid]').val();
		let pp_year = $('[name=pp_year]').val();
		let pp_month = $('[name=pp_month]').val();
		let pp_period = $('[name=pp_period]').val();

		let params = {
			employeeid: employeeid,
			pp_year: pp_year,
			pp_month: pp_month,
			pp_period: pp_period,
		}

		// get base url. Remove this if approver folder is the root folder
		let url = getBaseUrl() + 'insert_hour.php';

		fetch(url,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(params)
		}).then(res=>{
			location.reload();
		})
	})

	$(document).on('click','#unapprove-sched', function(){
		$('.modal-container').removeClass('hidden');

		let employeeid = $('[name=employeeid]').val();
		let pp_year = $('[name=pp_year]').val();
		let pp_month = $('[name=pp_month]').val();
		let pp_period = $('[name=pp_period]').val();

		let params = {
			employeeid: employeeid,
			pp_year: pp_year,
			pp_month: pp_month,
			pp_period: pp_period,
		}

		// get base url. Remove this if approver folder is the root folder
		let url = getBaseUrl() + 'unapprove.php';

		fetch(url,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(params)
		}).then(res=>{
			location.reload();
		})
	})

	$(document).on('click','.link',function(){
		let employeeid = $(this).parents('tr').find('[name=employeeid]').val()
		let pp_month = $(this).parents('tr').find('[name=pp_month]').val()
		let pp_period = $(this).parents('tr').find('[name=pp_period]').val()
		let pp_year = $(this).parents('tr').find('[name=pp_year]').val()

		// get base url. Remove this if approver folder is the root folder
		let url = getBaseUrl() + 'employee_timelog.php';

		url = `${url}?employeeid=${employeeid}&pp_month=${pp_month}&pp_period=${pp_period}&pp_year=${pp_year}`

		window.open(url);
		
	});

	$(document).on('change','[name=month], [name=year], [name=period]',function(){
		let month = $('[name=month]').val();
		let year = $('[name=year]').val();
		let period = $('[name=period]').val();

		let paramString = `month=${month}&year=${year}&period=${period}`

		location.search = paramString
	})

	function getBaseUrl(){
		let baseUrl = location.host + location.pathname
		let protocol = location.protocol

		baseUrl = protocol + '//' + baseUrl.replace(/\w+\.php/,'')

		return baseUrl
	}


	$(document).on('change','[name=pp_month], [name=pp_period], [name=pp_year]', function(){
		let parent = $(this).parents('tr')
		let pp_month = parent.find('[name=pp_month]').val()
		let pp_period = parent.find('[name=pp_period]').val()
		let pp_year = parent.find('[name=pp_year]').val()
		let employeeid = parent.find('[name=employeeid]').val()
		let lastTD = parent.find('td:last-child');

		// get base url. Remove this if approver folder is the root folder
		let url = getBaseUrl() + 'is_schedule_approved.php';

		$.ajax({
			url: url,
			method: 'get',
			data: {
				employeeid: employeeid,
				pp_year: pp_year,
				pp_month: pp_month,
				pp_period: pp_period
			},
			success: function(resp){
				if(resp.exist == 1){
					lastTD.html(`<i class='fa fa-check-square-o text-success' aria-hidden='true' style='font-size: 1.5rem !important'></i>`)
				}else{
					lastTD.html(`<i class='fa fa-close text-danger' aria-hidden='true'  style='font-size: 1.5rem !important'></i>`)
				}
			}
		})
	})
})
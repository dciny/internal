<?php

if(!isset($_SESSION)){
	session_start();
}

if(!isset($_SESSION['employeeid']))
	header("Location: /Internal/schedule/");


require_once __DIR__ . '/models/approved_payperiod.php';


$contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';

if($contentType === 'application/json'){
	$content = trim(file_get_contents('php://input'));
	$decoded = json_decode($content, true);

	$employee_pp = (object) [
		'employeeid' => trim($decoded['employeeid']),
		'pp_year' => trim($decoded['pp_year']),
		'pp_month' => trim($decoded['pp_month']),
		'pp_period' => trim($decoded['pp_period'])
	];
}else{
	$employee_pp = (object) [
		'employeeid' => isset($_POST['employeeid']) ? $_POST['employeeid'] : '',
		'pp_year' => isset($_POST['pp_year']) ? $_POST['pp_year'] : '',
		'pp_month' => isset($_POST['pp_month']) ? $_POST['pp_month'] : '',
		'pp_period' => isset($_POST['pp_period']) ? $_POST['pp_period'] : ''
	];
}


// unapprove payperiod
$full_pp = $employee_pp->pp_year . '-' . $employee_pp->pp_month . '-' . $employee_pp->pp_period;

$approved_pp = ApprovedPayperiod::init()
                                ->set_payperiod($full_pp)
                                ->set_employeeid($employee_pp->employeeid);

$approved_pp->delete();
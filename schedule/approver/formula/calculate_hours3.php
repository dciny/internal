<?php

/* Get the Evening ND 10pm - 12am

initialize:
1.) set 12am = 12am of enddate. set 10pm = 10pm startdate.
Loop:
2.) Loop while hour is lesser than or equal to 10pm and hour is lesser than or equal to enddate
outside loop:
3.) if hour is greater than or equal to 10pm, then execute step 4 (meaning it has date within 10pm - 12am)
4.) get hour without any minutes
5.) if date at 12am is lesser than or equal to enddate then execute step 5, else execute step 6
6.) let 12am hour set to date at 12am
7.) let 12am hour set to date at enddate
8.) get the difference between step 4 and (step 6 or step 7).

*/


function calcEveningND(&$hourStorage, $startdate, $enddate){
	$dateAt10Pm = date('Y-m-d 22:00:00', strtotime($startdate));
	$dateAt12Am = date('Y-m-d 00:00:00', strtotime($dateAt10Pm . '+2 hours'));
	$enddate = date('Y-m-d H:i:s', strtotime($enddate)); // make sure enddate is of type date

	$hour = date('Y-m-d H:i:s', strtotime($startdate)); // counter to use

	while($hour < $dateAt10Pm && $hour <= $enddate)
		$hour = date('Y-m-d H:i:s', strtotime($hour . '+1 hour'));

	if($hour >= $dateAt10Pm && $hour <= $dateAt12Am){
		$hour = $startdate < $hour ? $dateAt10Pm : $hour;
		$dateAt12Am = $dateAt12Am <= $enddate ? $dateAt12Am : $enddate;
		$offset = round((strtotime($dateAt12Am) - strtotime($hour)) / 3600, 2);
		$date_idx = date('Y-m-d', strtotime($startdate));

		if(isset($hourStorage[$date_idx]) && isset($hourStorage[$date_idx]['ndHrs'])){
			$hourStorage[$date_idx]['ndHrs'] = $hourStorage[$date_idx]['ndHrs'] + $offset;
		}else{
			$hourStorage[$date_idx] = ['ndHrs' => $offset];
		}
	}
}


function calcMorningND(&$hourStorage, $startdate, $enddate){
	$startdate = date('Y-m-d H:i:s', strtotime($startdate));
    $enddate = date('Y-m-d H:i:s', strtotime($enddate));
    
    $dateAt12Am = date('Y-m-d 00:00:00', strtotime($enddate));
    $dateAt6Am = date('Y-m-d H:i:s', strtotime($dateAt12Am . '+6 hours'));
    
    $offset = 0;
    if($startdate < $dateAt6Am){
         $from = $startdate > $dateAt12Am ? $startdate : $dateAt12Am;
         $to = $enddate > $dateAt6Am ? $dateAt6Am : $enddate;
         
         $offset = round((strtotime($to) - strtotime($from)) / 3600, 2);
         $date_idx = date('Y-m-d', strtotime($enddate));
         
         if(isset($hourStorage[$date_idx]['ndHrs'])){
			$hourStorage[$date_idx]['ndHrs'] = $hourStorage[$date_idx]['ndHrs'] + $offset;
		}else{
			$hourStorage[$date_idx] = ['ndHrs' => $offset];
		}
    }
}


function calcRegHrs(&$hourStorage, $startdate, $enddate){
	$startdate = date('Y-m-d H:i:s', strtotime($startdate));
	$enddate = date('Y-m-d H:i:s', strtotime($enddate));

	$dateAt6Am = date('Y-m-d 06:00:00', strtotime($startdate));
	$dateAt10Pm = date('Y-m-d 22:00:00', strtotime($startdate));
	$dateAt12Am = date('Y-m-d H:i:s', strtotime($dateAt10Pm . '+2 hours'));

	$from = $startdate < $dateAt6Am ? $dateAt6Am : $startdate;
	$until = $enddate < $dateAt10Pm ? $enddate : $dateAt10Pm;

	$offset = round((strtotime($until) - strtotime($from)) / 3600, 2);
	$offset = $offset < 0 ? 0 : $offset;
	$date_idx = date('Y-m-d', strtotime($startdate));

	if(isset($hourStorage[$date_idx]['regHrs'])){
	  $hourStorage[$date_idx]['regHrs'] = $hourStorage[$date_idx]['regHrs'] + $offset;
	}else{
	  $hourStorage[$date_idx]['regHrs'] = $offset;
	}


	if($enddate > $dateAt12Am){
	  $dateAt6am = date('Y-m-d H:i:s', strtotime($dateAt12Am . '+6 hours'));
	  if($enddate > $dateAt6am){
	     $offset = round((strtotime($enddate) - strtotime($dateAt6am)) / 3600,2);
	     $date_idx = date('Y-m-d', strtotime($enddate));
	     if(isset($hourStorage[$date_idx]['regHrs'])){
	        $hourStorage[$date_idx]['regHrs'] = $hourStorage[$date_idx]['regHrs'] + $offset;
	     }else{
	        $hourStorage[$date_idx]['regHrs'] = $offset;
	     }
	  }
	}
}


<?php

function calcCheckPointHour(&$hours, $date, $dateAtCheckPointHr, $reghrs=false){
	
	// compute for ND hour
	$date_idx = date('Y-m-d',strtotime($date));
	$offset = date('G', strtotime($date)) == 6 ? 1 - (date('i',strtotime($date)) / 60) : (date('i',strtotime($date)) / 60);
	// sum if exist, add new date if not exist for nightdiff hours
	if(isset($hours['ndHrs'][$date_idx]))
		$hours['ndHrs'][$date_idx] = $hours['ndHrs'][$date_idx] + $offset;
	else
		$hours['ndHrs'][$date_idx] = $offset;
	

	// compute for regular hour OR ND if checkpoint hour is at 12am
	if($reghrs || $dateAtCheckPointHr == date('Y-m-d 00:00:00', strtotime($date))){
		$idx = $reghrs ? 'regHrs' : 'ndHrs';
		$prevStartdate = date('Y-m-d H:i:s', strtotime($date . '-1 hour'));
		$date_idx = date('Y-m-d',strtotime($prevStartdate));
		$offsetAtCheckPointHr = (strtotime($dateAtCheckPointHr) - strtotime($prevStartdate)) / 3600;
		if(isset($hours[$idx][$date_idx]))
			$hours[$idx][$date_idx] = $hours[$idx][$date_idx] + $offsetAtCheckPointHr;
		else
			$hours[$idx][$date_idx] = $offsetAtCheckPointHr;
	}
}

function addInsertHour($date, &$hours, $idx){
	$date_idx = date('G',strtotime($startdate)) == 0 ? date('Y-m-d',strtotime($date . '-1 hour')) : date('Y-m-d',strtotime($date));
	if(isset($hours[$idx][$date_idx]))
		$hours[$idx][$date_idx] = $hours[$idx][$date_idx] + 1; 
	else
		$hours[$idx][$date_idx] = 1;
}



// =============================================================================================

$dateAt10Pm = date('Y-m-d 22:00:00',strtotime($startdate));
$dateAt6Am = date('Y-m-d 06:00:00',strtotime($enddate));
$origStartDate = date('Y-m-d H:i:s',strtotime($startdate));

$hours = [
	'regHrs' => [],
	'ndHrs'  => []
];


$startdate = date('Y-m-d H:i:s', strtotime($startdate . '+1 hour'));
while($startdate <= $enddate){

	if(date('G',strtotime($startdate)) == 22 && date('i',strtotime($startdate)) > 0){
		calcCheckPointHour($hours, $startdate, $dateAt10Pm, true);
	}elseif(date('G',strtotime($startdate)) == 0 && date('i',strtotime($startdate)) > 0){
		$dateAt12Am = date('Y-m-d 00:00:00', strtotime($startdate));
		calcCheckPointHour($hours, $startdate, $dateAt12Am);
	}elseif(date('G',strtotime($startdate)) == 6 && date('i',strtotime($startdate)) > 0){
		calcCheckPointHour($hours, $startdate, $dateAt6Am, true);
	}elseif(in_array(date('G',strtotime($startdate)),[0,23,1,2,3,4,5,6])){
		addInsertHour($startdate, $hours, 'ndHrs');
	}else{
		addInsertHour($startdate, $hours, 'regHrs');
	}

	$startdate = date('Y-m-d H:i:s', strtotime($startdate . '+1 hour'));
}

if(date('i',strtotime($startdate)) != date('i',strtotime($enddate))){
	$prevStartDate = date('Y-m-d H:i:s', strtotime($startdate . '-1 hour'));
	$offset = (strtotime($enddate) - strtotime($prevStartDate)) / 3600;
	
	$idx = in_array(date('G',strtotime($prevStartDate)), [22,23,0,1,2,3,4,5]) ? 'ndHrs' : 'regHrs';
	$date_idx = date('Y-m-d',strtotime($prevStartDate));

	if(isset($hours[$idx][$date_idx]))
		$hours[$idx][$date_idx] = $hours[$idx][$date_idx] + $offset;
	else
		$hours[$idx][$date_idx] = $offset;
}
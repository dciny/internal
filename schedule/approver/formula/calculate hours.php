<?php

private function calculate_evening_nightdiff($startdate, $enddate){
	$evening_night_diff_hrs = 0;
	$nextdayAt12am = date('Y-m-d 00:00:00', strtotime($startdate . '+1 day'));
	$enddate = date('Y-m-d H:i:s',strtotime($enddate));

	$startdate = date('Y-m-d H:i:s', strtotime($startdate . '+1 hour'));
	while($startdate <= $nextdayAt12am){
		if($startdate > $enddate)
			break;

		if(date('G',strtotime($startdate)) == 22 && date('i',strtotime($startdate)) > 0)
			$evening_night_diff_hrs = $evening_night_diff_hrs + (date('i',strtotime($startdate)) / 60);

		if(in_array(date('G',strtotime($startdate)),[23,0]))
			 $evening_night_diff_hrs = $evening_night_diff_hrs + 1;

		$startdate = date('Y-m-d H:i:s', strtotime($startdate . '+1 hour'));
	}

	if($startdate <= $enddate AND date('i',strtotime($startdate)) > 0){
		$startdate_dtHr_only = date('Y-m-d H:00:00',strtotime($startdate));
		$ceiling = date('Y-m-d H:i:s',strtotime($startdate_dtHr_only . '+1 hour'));
		$ceiling_startdate_diff_sec = strtotime($ceiling) - strtotime($startdate);
		$evening_night_diff_hrs = $evening_night_diff_hrs + ($ceiling_startdate_diff_sec / 3600);
	}else{
		if($enddate <= $nextdayAt12am){
			$startdt_min = date('i',strtotime($startdate));
			$enddt_min = date('i',strtotime($enddate));
			if($startdt_min - $enddt_min != 0){
				$startdt1hrbefore = date('Y-m-d H:i:s',strtotime($startdate . '-1 hour'));
				$diff_in_sec = strtotime($enddate) - strtotime($startdt1hrbefore);
				$evening_night_diff_hrs = $evening_night_diff_hrs + ($diff_in_sec / 3600);
			}
		}	
	}

	return round($evening_night_diff_hrs,2,PHP_ROUND_HALF_UP);
}








$dateAt10Pm = date('Y-m-d 22:00:00',strtotime($startdate));
$dateAt6Am = date('Y-m-d 06:00:00',strtotime($enddate));
$origStartDate = date('Y-m-d H:i:s',strtotime($startdate));

$hours = [
	'regHrs' => [],
	'ndHrs'  => []
];

$startdate = date('Y-m-d H:i:s', strtotime($startdate . '+1 hour'));
while($startdate <= $enddate){
	
	// check if within 10pm and minutes is greater than 0
	if(date('G',strtotime($startdate)) == 22 && date('i',strtotime($startdate)) > 0){
		$date_idx = date('Y-m-d',strtotime($startdate));
		// sum if exist, add new date if not exist for nightdiff hours
		if(isset($hours['ndHrs'][$date_idx]))
			$hours['ndHrs'][$date_idx] = $hours['ndHrs'][$date_idx] + (date('i',strtotime($startdate)) / 60);
		else
			$hours['ndHrs'][$date_idx] = date('i',strtotime($startdate)) / 60;

		// compute for regular hour
		$prevStartdate = date('Y-m-d H:i:s', strtotime($startdate . '-1 hour'));
		$offsetAt10pm = (strtotime($dateAt10Pm) - strtotime($prevStartdate)) / 3600;
		if(isset($hours['regHrs'][$date_idx]))
			$hours['regHrs'][$date_idx] = $hours['regHrs'][$date_idx] + $offsetAt10pm;
		else
			$hours['regHrs'][$date_idx] = $offsetAt10pm;
	}elseif(date('G',strtotime($startdate)) == 0 && date('i',strtotime($startdate)) > 0){
		$dateAt12Am = date('Y-m-d 00:00:00', $startdate);
		$prevStartdate = date('Y-m-d H:i:s', strtotime($startdate . '-1 hour'));
		$offset_12am = (strtotime($dateAt12Am) - strtotime($prevStartdate)) / 3600;
		$date_idx = date('Y-m-d',strtotime($prevStartdate));
		if(isset($hours['ndHrs'][$date_idx]))
			$hours['ndHrs'][$date_idx] = $hours['ndHrs'][$date_idx] + $offset_12am;
		else
			$hours['ndHrs'][$date_idx] = $offset_12am;
	}elseif(date('G',strtotime($startdate)) == 0){
		$date_idx = date('Y-m-d',strtotime($startdate . '-1 hour'));
		if(isset($hours['ndHrs'][$date_idx]))
			$hours['ndHrs'][$date_idx] = $hours['ndHrs'][$date_idx] + 1; 
		else
			$hours['ndHrs'][$date_idx] = 1;
	}elseif(date('G',strtotime($startdate)) == 6 && date('i',strtotime($startdate)) > 0){
		$date_idx = date('Y-m-d',strtotime($enddate));
		// sum if exist, add new date if not exist for nightdiff hours
		if(isset($hours['ndHrs'][$date_idx]))
			$hours['ndHrs'][$date_idx] = $hours['ndHrs'][$date_idx] + (date('i',strtotime($startdate)) / 60);
		else
			$hours['ndHrs'][$date_idx] = date('i',strtotime($startdate)) / 60;

		// compute for regular hour
		$prevStartdate = date('Y-m-d H:i:s', strtotime($startdate . '-1 hour'));
		$offsetAt6am = (strtotime($dateAt6Am) - strtotime($prevStartdate)) / 3600;
		if(isset($hours['regHrs'][$date_idx]))
			$hours['regHrs'][$date_idx] = $hours['regHrs'][$date_idx] + $offsetAt6am;
		else
			$hours['regHrs'][$date_idx] = $offsetAt6am;
	}elseif(in_array(date('G',strtotime($startdate)),[0,23,1,2,3,4,5,6])){
		$date_idx = date('Y-m-d',strtotime($startdate));
		if(isset($hours['ndHrs'][$date_idx]))
			$hours['ndHrs'][$date_idx] = $hours['ndHrs'][$date_idx] + 1; 
		else
			$hours['ndHrs'][$date_idx] = 1;
	}else{
		$date_idx = date('Y-m-d',strtotime($startdate));
		if(isset($hours['regHrs'][$date_idx]))
			$hours['regHrs'][$date_idx] = $hours['regHrs'][$date_idx] + 1; 
		else
			$hours['regHrs'][$date_idx] = 1;
	}

	$startdate = date('Y-m-d H:i:s', strtotime($startdate . '+1 hour'));
}


if()
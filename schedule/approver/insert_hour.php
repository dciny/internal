<?php

if(!isset($_SESSION)){
	session_start();
}

if(!isset($_SESSION['employeeid']))
	header("Location: /Internal/schedule/");


require_once __DIR__ . '/models/timelog.php';
require_once __DIR__ . '/models/hourstable.php';
require_once __DIR__ . '/models/finalhourstable.php';
require_once __DIR__ . '/models/approved_payperiod.php';


$contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';

if($contentType === 'application/json'){
	$content = trim(file_get_contents('php://input'));
	$decoded = json_decode($content, true);

	$employee_pp = (object) [
		'employeeid' => trim($decoded['employeeid']),
		'pp_year' => trim($decoded['pp_year']),
		'pp_month' => trim($decoded['pp_month']),
		'pp_period' => trim($decoded['pp_period'])
	];
}else{
	$employee_pp = (object) [
		'employeeid' => isset($_POST['employeeid']) ? $_POST['employeeid'] : '',
		'pp_year' => isset($_POST['pp_year']) ? $_POST['pp_year'] : '',
		'pp_month' => isset($_POST['pp_month']) ? $_POST['pp_month'] : '',
		'pp_period' => isset($_POST['pp_period']) ? $_POST['pp_period'] : ''
	];
}

header('Content-Type: json/application');

// echo json_encode($employee_pp);

$employee_timelog = new Timelog($employee_pp);
$timelogs = $employee_timelog->get_payperiod_timelog();

// $rows contains the table format values for all columns in hourstable and finalhourstable
$rows = $employee_timelog->hourstable_to_insert();

// use $rows to insert both hourstable and finalhourstable

foreach ($rows as $row) {
	// insert or update hourstable
	$hrstbl_row = HoursTable::init();
	$hrstbl_row->set_shiftdate($row->shiftdate)
	           ->set_dates($row->dates)
	           ->set_userid($row->userid)
	           ->set_regularHours($row->regular_hours)
	           ->set_nightdiffHours($row->nightdiff_hours)
	           ->set_regularOtHours($row->regular_ot_hours)
	           ->set_nightdiffOtHours($row->nightdiff_ot_hours)
	           ->set_late($row->late)
	           ->set_undertime($row->undertime)
	           ->set_status($row->status);

	if($hrstbl_row->is_exist() > 0){
		$hrstbl_row->update();
	}else{
		$hrstbl_row->insert();
	}


	// insert or update finalhourstable
	$finhrstbl_row = FinalHoursTable::init();
	$finhrstbl_row->set_shiftdate($row->shiftdate)
	              ->set_dates($row->dates)
	              ->set_userid($row->userid)
	              ->set_regularHours($row->regular_hours)
	              ->set_nightdiffHours($row->nightdiff_hours)
	              ->set_regularOtHours($row->regular_ot_hours)
	              ->set_nightdiffOtHours($row->nightdiff_ot_hours)
	              ->set_late($row->late)
	              ->set_undertime($row->undertime)
	              ->set_status($row->status);

	if($finhrstbl_row->is_exist() > 0){
		$finhrstbl_row->update();
	}else{
		$finhrstbl_row->insert();
	}

}

// insert or update approvedpayperiods table
$full_pp = $employee_pp->pp_year . '-' . $employee_pp->pp_month . '-' . $employee_pp->pp_period;

$approved_pp = ApprovedPayperiod::init()
                                ->set_payperiod($full_pp)
                                ->set_employeeid($employee_pp->employeeid);

if($approved_pp->is_exist() > 0){
	$approved_pp->update();
}else{
	$approved_pp->insert();
}
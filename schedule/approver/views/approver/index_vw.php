
<!DOCTYPE html>
<html>
<head>
	<title>Members</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- Font Awesome 4.7 -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- custom styles -->
	<link rel="stylesheet" type="text/css" href="./css/styles.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" defer></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>

	<script src="./js/scripts.js" defer></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card p-3 my-3">
					<div class="d-flex align-items-center my-2" style="gap: 0.5rem">
						<h5>Payperiod</h5>
						<div class="d-flex" style="gap: 0.5rem">
							<select name="month" class="form-control">
								<?php 
									foreach ($month_options as $month_option) {
										$selected = selected($month_param,$month_option->mo);
										echo "<option value='{$month_option->mo}' $selected>{$month_option->month}</option>";
									}
								?>
							</select>
							<select name="period" class="form-control">
								<option value="10" <?php echo $period_param == 10 ? 'selected' : '' ?>>10</option>
								<option value="25" <?php echo $period_param == 25 ? 'selected' : '' ?>>25</option>
							</select>
							<select name="year" class="form-control">
								<?php 
									foreach ($year_options as $year) {
										$selected = selected($year_param,$year);
										echo "<option value='$year' $selected>$year</option>";
									}
								?>
							</select>
						</div>
					</div>
					<table class="table table-sm table-bordered table-responsive w-100 d-block d-md-table">
						<thead class="thead-yellow">
							<tr>
								<th>Employee ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Month</th>
								<th>Pay Period</th>
								<th>Year</th>
								<th>View</th>
								<th>Approved?</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($members as $member){ ?>
								<tr>
									<td><?php echo isset($member->employeeid) ? $member->employeeid : '' ?></td>
									<td><?php echo isset($member->firstname) ? $member->firstname : '' ?></td>
									<td><?php echo isset($member->lastname) ? $member->lastname : '' ?></td>
									<td>
										<select class="form-control" name="pp_month">
											<?php 
												foreach ($month_options as $month_option) {
													$selected = selected($month_param,$month_option->mo);
													echo "<option value='{$month_option->mo}' $selected>{$month_option->month}</option>";
												}
											?>
										</select>
									</td>
									<td>
										<select class="form-control" name="pp_period">
											<option value="10" <?php echo $period_param == 10 ? 'selected' : '' ?>>10</option>
											<option value="25" <?php echo $period_param == 25 ? 'selected' : '' ?>>25</option>
										</select>
									</td>
									<td>
										<select class="form-control" name="pp_year">
											<?php 
												foreach ($year_options as $year) {
													$selected = selected($year_param,$year);
													echo "<option value='$year' $selected>$year</option>";
												}
											?>
										</select>
									</td>
									<td class="text-center">
										<i class="fa fa-search link" aria-hidden="true"></i>
										<input type="hidden" name="employeeid" value="<?php echo $member->employeeid ?>">
									</td>
									<td class="text-center">
										<?php if(in_array($member->employeeid, $members_with_approved_sched)){ ?>
											<i class="fa fa-check-square-o text-success" aria-hidden="true" style="font-size: 1.5rem !important"></i>
										<?php }else{ ?>
											<i class="fa fa-close text-danger" aria-hidden="true"  style="font-size: 1.5rem !important"></i>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>
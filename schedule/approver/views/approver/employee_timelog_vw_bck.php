<!-- Controller: approver_scheduler/employee_timelog.php -->
<!DOCTYPE html>
<html>
<head>
	<title>
		<?php 
			if(!empty($calendar_header))
				echo $employee->employeeid . ' - ' . implode(' ', array($employee->firstname, $employee->lastname));
			else
				echo 'Incomplete parameter';
		?>
	</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- Font Awesome 4.7 -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- custom styles -->
	<link rel="stylesheet" type="text/css" href="./css/styles.css">

	<!-- Date Picker -->
	<link rel="stylesheet" type="text/css" href="./css/bootstrap-datetimepicker.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" defer></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>

	<script src="/js/moment.js" defer></script>

	<script src="/js/bootstrap-datetimepicker.min.js" defer></script>

	<script src="/js/scripts.js" defer></script>
</head>
<body>

	<div class="modal-container hidden">
		<div class="modal">
			<div class="d-flex flex-column">
				<span class="text-center">Please Wait</span>
				<div>
					<div class="spinner-grow text-info"></div>
					<div class="spinner-grow text-warning"></div>
					<div class="spinner-grow text-danger"></div>
				</div>
			</div>	
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12 p-3">
				<div class="card p-3">
					<div class="d-flex flex-wrap justify-content-between">
						<?php if(!empty($calendar_header)) { ?>
							<h5>
								<i class="fa fa-calendar mr-2 text-info"></i>
								<?php echo $calendar_header->month . ' ' . implode(' - ', [$calendar_header->first, $calendar_header->last]) . ' ' . $calendar_header->year ?>
							</h5>
							<h5>
								<i class="fa fa-user-circle mr-2 text-info"></i>
								<?php echo $employee->employeeid . ' - ' . implode(' ', array($employee->firstname, $employee->lastname)) ?>
							</h5>
						<?php } else{ ?>
							<h5>
								<i class="fa fa-calendar mr-2 text-info"></i>
								Incomplete parameter
							</h5>
							<h5>
								<i class="fa fa-user-circle mr-2 text-info"></i>
								Incomplete parameter
							</h5>
						<?php } ?>
					</div>

					<table class="timelog-table table table-sm table-bordered table-responsive w-100 d-block d-md-table">
						<thead class="thead-yellow">
							<tr class="text-center">
								<th colspan="2">Day</th>
								<th>Status</th>
								<th>Start Shift</th>
								<th>End Shift</th>
								<th>OT Start</th>
								<th>OT End</th>
								<th>Shift</th>
							</tr>
						</thead>
						<tbody id="timelog-tbl">
							<?php if(!empty($employee_pp_timelog)) { ?>
								<?php foreach ($employee_pp_timelog as $timelog) { ?>
									<tr class="text-center <?php echo day_status_style($timelog->status) ?>">
										<td>
											<?php echo $timelog->shiftday ?>
											<input type="hidden" name="day" value="<?php echo $timelog->day ?>" data-day>
											<input type="hidden" name="shiftday" value="<?php echo $timelog->shiftday ?>" data-shiftday>
											<input type="hidden" name="userid" value="<?php echo $timelog->userid ?>" data-userid>		
										</td>
										<td><?php echo $timelog->dates ?></td>
										<td class="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'input' ?>">
											<span data-toggle="tooltip" data-placement="right" title="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'Double click to edit' ?>">
												<?php echo $timelog->status ?>
											</span>
											<?php if(is_editable($timelog->status)){ ?>
												<div class="hidden">
													<div class="datetime d-flex justify-content-center align-items-center">
														<select class="form-control" name="date[<?php echo $timelog->dates ?>][status]" data-status>
															<?php 
																foreach ($timelogStatuses as $timelogStatus) { 
																	$selected = $timelogStatus->name == $timelog->status ? 'selected' : '';
																	echo "<option value='$timelogStatus->name' $selected>$timelogStatus->name</option>";
																}
															?>
														</select>
													</div>
												</div>
											<?php }else{ ?>	
												<input type="hidden" name="date[<?php echo $timelog->dates ?>][status]" value="<?php echo $timelog->status ?>" data-status>
											<?php } ?>
										</td>
										<td class="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'input' ?>">
											<span data-toggle="tooltip" data-placement="right" title="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'Double click to edit' ?>">
												<?php echo $timelog->startshift ?>
											</span>
											<div class="hidden">
												<div class="datetime d-flex justify-content-center align-items-center">
													<div style="position: relative;">
														<input type="text" class="form-control datetmpicker" value="<?php echo datetimeLocal($timelog->startshift) ?>">
													</div>
													<input type="hidden" 
													       name="date[<?php echo $timelog->dates ?>][startshift]" 
													       value="<?php echo $timelog->startshift ?>" data-startshift>
												</div>
											</div>
										</td>
										<td class="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'input' ?>">
											<span data-toggle="tooltip" data-placement="right" title="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'Double click to edit' ?>">
												<?php echo $timelog->endshift ?>
											</span>
											<div class="hidden">
												<div class="datetime d-flex justify-content-center align-items-center">
													<input type="datetime-local" class="form-control" value="<?php echo datetimeLocal($timelog->endshift) ?>">
													<input type="hidden" 
													       name="date[<?php echo $timelog->dates ?>][endshift]" 
													       value="<?php echo $timelog->endshift ?>" data-endshift>
												</div>
											</div>
										</td>
										<td class="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'input-empty' ?> <?php echo displayDateThatCanBeEmpty($timelog->otstart) == '0000-00-00 00:00:00' || $is_approved || !is_editable($timelog->status) ? '' : 'removeOt' ?>">
											<span data-toggle="tooltip" data-placement="left" title="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'Double click to edit' ?>">
												<?php echo displayDateThatCanBeEmpty($timelog->otstart) ?>
											</span>
											<i class="OtEl fa fa-close"></i>
											<div class="hidden">
												<div class="datetime d-flex justify-content-center align-items-center">
													<?php echo htmlForDateThatCanBeEmpty($timelog->otstart,'otstart','form-control') ?>
													<input type="hidden" 
														   name="date[<?php echo $timelog->dates ?>][otstart]" 
														   value="<?php echo standardDateFormat($timelog->otstart) ?>" data-otstart>
												</div>
											</div>
										</td>
										<td class="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'input-empty' ?> <?php echo displayDateThatCanBeEmpty($timelog->otend) == '0000-00-00 00:00:00' || $is_approved || !is_editable($timelog->status) ? '' : 'removeOt' ?>">
											<span data-toggle="tooltip" data-placement="left" title="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'Double click to edit' ?>">
												<?php echo displayDateThatCanBeEmpty($timelog->otend) ?>
											</span>
											<i class="OtEl fa fa-close"></i>
											<div class="hidden">
												<div class="datetime d-flex justify-content-center align-items-center">
													<?php echo htmlForDateThatCanBeEmpty($timelog->otend,'otend','form-control') ?>
													<input type="hidden" 
														   name="date[<?php echo $timelog->dates ?>][otend]" 
														   value="<?php echo standardDateFormat($timelog->otend) ?>" data-otend>
												</div>
											</div>	
										</td>
										<td class="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'input' ?>">
											<span data-toggle="tooltip" data-placement="right" title="<?php echo $is_approved || !is_editable($timelog->status) ? '' : 'Double click to edit' ?>">
												<?php echo $timelog->thisdaysshift ?>
											</span>
											<div class="hidden">
												<div class="datetime d-flex justify-content-center align-items-center">
													<select class="form-control" name="date[<?php echo $timelog->dates ?>][shift]" data-shift>
														<option value=" ">&nbsp;</option>
														<?php 
															foreach ($shifts as $shift) { 
																$selected = $shift->groupschedulename == $timelog->thisdaysshift ? 'selected' : '';
																echo "<option value='$shift->groupschedulename' $selected>$shift->groupschedulename</option>";
															}
														?>
													</select>
												</div>
											</div>
										</td>
									</tr>
								<?php } ?>
							<?php }else{ ?>
								<tr class="text-center">
									<td colspan="8">Empty Result</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<div class="d-flex justify-content-end">
						<?php if(!$is_approved) { ?>
							<button id="create-timelog-btn" class="btn btn-sm btn-info">Plot Schedule</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row my-3">
			<div class="col-md-12">
				<div class="card p-3">
					<div class="d-flex justify-content-between">
						<h5>
							<i class="fa fa-calendar-check-o mr-2 text-info"></i>
							Summary Hours Table
						</h5>
						<h5>
							Status: 
							<span class="badge <?php echo $is_approved ? 'badge-success' : 'badge-warning' ?>">
								<?php echo $is_approved ? 'Approved' : 'Pending for Approval' ?>
							</span>
						</h5>
					</div>
					<div class="pos-relative min-height-12rem">
						<table class="table table-sm table-bordered table-responsive w-100 d-block d-md-table">
							<thead class="thead thead-dark">
								<tr class="text-center">
									<th>Date</th>
									<th>Reg H</th>
									<th>W/ ND</th>
									<th>Late</th>
									<th>Undertime</th>
									<th>Regular OT</th>
									<th>OT W/ ND</th>
								</tr>
							</thead>
							<tbody>
								<?php if(isset($summary_rows)) { ?>
									<?php foreach($summary_rows as $row) { ?>
										<tr class="text-center <?php echo day_status_style($row->status) ?>">
											<td><?php echo $row->day ?></td>
											<td><?php echo $row->regHrs ?></td>
											<td>
												<span><?php echo $row->withND ?></span>
											</td>
											<td><?php echo $row->late ?></td>
											<td><?php echo $row->undertime ?></td>
											<td><?php echo $row->regOt ?></td>
											<td><?php echo $row->otWithND ?></td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
										<td colspan="9">Incomplete parameter</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<div class="d-flex justify-content-end py-4">
						<?php if(!empty($employee_pp->employeeid) && !empty($employee_pp->pp_year) && !empty($employee_pp->pp_month) && !empty($employee_pp->pp_period)){ ?>
							<?php if(!$is_approved) { ?>
								<button id="approve-sched" class="btn btn-sm btn-primary">Approve Schedule</button>
							<?php }else{ ?>
								<button id="unapprove-sched" class="btn btn-sm btn-danger">Unapprove Schedule</button>
							<?php } ?>
							<input type="hidden" name="employeeid" value="<?php echo $employee_pp->employeeid ?>">
							<input type="hidden" name="pp_year" value="<?php echo $employee_pp->pp_year ?>">
							<input type="hidden" name="pp_month" value="<?php echo $employee_pp->pp_month ?>">
							<input type="hidden" name="pp_period" value="<?php echo $employee_pp->pp_period ?>">
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>
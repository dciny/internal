<?php
require('dbconnect.php');

$month = $_POST['month'];
$year = $_POST['year'];
$payperiod = $_POST['payperiod'];
$employeeids = isset($_POST['employeeids']) ? $_POST['employeeids'] : [];

$full_pp = implode('-', [$year, $month, $payperiod]);


// remove from approved_payperiods table
$q1 = "DELETE FROM approved_payperiods WHERE employeeid IN('" . implode("','", $employeeids) . "') AND payperiod = '$full_pp'";


// remove from finalhourstable table
$shiftdate = '';
if($payperiod == 10){
	$shiftdate = date('Y-m-16', strtotime(date('Y-m-d',strtotime($full_pp)).' -1 MONTH'));
	$enddate = date("Y-m-t",strtotime($shiftdate));
}else{
	$shiftdate =  date('Y-m-01', strtotime($full_pp));
	$enddate = date('Y-m-15', strtotime($full_pp));
}

$q2 = "DELETE FROM finalhourstable WHERE userid IN('" . implode("','", $employeeids) . "') AND shiftdate >= '$shiftdate' AND shiftdate <= '$enddate'";



$conn->query($q1);
$q1_affected_rows = $conn->affected_rows;

$conn->query($q2);
$q2_affected_rows = $conn->affected_rows;


$conn->close();


echo "approved_payperiods: $q1_affected_rows   |   finalhourstable: $q2_affected_rows";
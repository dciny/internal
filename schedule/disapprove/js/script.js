$(document).ready(function(){
	
	$(document).on('click','.add',function(){
		var row = $(this).parents('tr');
		var employeeid = row.find('td:nth-child(1)').text().trim();
		var name = row.find('td:nth-child(2)').text().trim();
		var newlist = generateAddedList(employeeid, name);

		$(newlist).appendTo('#emp-added');
		row.remove();

		sortElem('#emp-added');
	});

	$(document).on('click','.remove',function(){
		var row = $(this).parents('tr');
		var employeeid = row.find('td:nth-child(1)').text().trim();
		var name = row.find('td:nth-child(2)').text().trim();
		var newlist = generateEmployeeList(employeeid, name);

		$(newlist).appendTo('#emp-list');
		row.remove();

		sortElem('#emp-list');
	});

	$(document).on('click','#searchEmp',function(){
		var searchTxt = $(this).parent().siblings('input').val();
		search('#emp-list',searchTxt);
	});

	$(document).on('click','#searchAdded',function(){
		var searchTxt = $(this).parent().siblings('input').val();
		search('#emp-added',searchTxt);
	});

	$(document).on('submit','form',function(e){
		e.preventDefault();

		var url = $(this).attr('action');
		var method = $(this).attr('method');

		var formObj = {employeeids: []};
		$(this).find('select').each(function(){
			formObj[this.name] = $(this).val();
		});

		$('#emp-added').children().each(function(){
			var employeeid = $(this).find('td:nth-child(1)').text().trim();
			formObj.employeeids.push(employeeid);
		});

		$.ajax({
			url: url,
			method: method,
			data: formObj,
			success: function(response){
				$('#message').append("<span class='alert alert-info'>"+ response +"</span>");
			}
		});
	});



	function sortElem(selector){
		$(selector).children().sort(function(secnd_el, frst_el){
			var sec_text = $(secnd_el).find('td:nth-child(2)').text().trim();
			var frst_text = $(frst_el).find('td:nth-child(2)').text().trim();

			return sec_text.localeCompare(frst_text);
		}).appendTo(selector);
	}

	function search(selector,searchTxt){
		var stxt = searchTxt.toLowerCase();

		$(selector).find('tr').removeClass('d-none').addClass('d-none');
		$(selector).children().each(function(){
			var employeeid = $(this).find('td:nth-child(1)').text().trim().toLowerCase();
			var name = $(this).find('td:nth-child(2)').text().trim().toLowerCase();

			if(employeeid.includes(stxt) || name.includes(stxt)){
				$(this).removeClass('d-none');
			}
		});
	}

	function generateAddedList(employeeid, name){
		return `
			<tr>
				<td class="align-middle">{employeeid}</td>
				<td class="align-middle">{name}</td>
				<td class="align-middle"><button class="btn btn-sm btn-danger remove">Remove</button></td>
			</tr>
		`.replace('{employeeid}',employeeid).replace('{name}',name);
	}

	function generateEmployeeList(employeeid, name){
		return `
			<tr>
				<td class="align-middle">{employeeid}</td>
				<td class="align-middle">{name}</td>
				<td class="align-middle"><button class="btn btn-sm btn-success add">Add</button></td>
			</tr>
		`.replace('{employeeid}',employeeid).replace('{name}',name);
	}
});